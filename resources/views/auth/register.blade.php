@extends('layouts.master')

@section('main-content')
@include('admin.includes.breadcrumb',[
        'title' => 'Les utilisateurs'])

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        @if ($errors->count()>0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-card alert-danger" role="alert">
                                    <strong class="text-capitalize">Erreur!</strong>
                                        {{ $error }}
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                </div>
                            @endforeach
                        @endif
                        <div class="card-title mb-3">Ajouter un administrateur</div>
                        <form method="POST" action="{{ route('admin.register') }}" enctype="multipart/form-data" >

                            @csrf
                            <div class="row">
                                <div class="col-md-6 form-group mb-3">
                                    <label for="firstname">Prénom</label>
                                    <input name="firstname" type="text" class="form-control form-control-rounded" placeholder="Entrez le prénom">
                                </div>

                                <div class="col-md-6 form-group mb-3">
                                    <label for="lastname">Nom</label>
                                    <input name="lastname" type="text" class="form-control form-control-rounded"  placeholder="Enter le  nom">
                                </div>

                                <div class="col-md-6 form-group mb-3">
                                    <label for="username">Username</label>
                                    <input name="username" type="text" class="form-control form-control-rounded" placeholder="Entrez le username">
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="mairie_id">Mairie</label>
                                    <select name="mairie_id" class="form-control form-control-rounded">
                                        @foreach ($mairies as $mairie)
                                            <option value="{{ $mairie->id }}" >{{ $mairie->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 form-group mb-3">
                                    <label for="email">Email address</label>
                                    <input name="email" type="email" class="form-control form-control-rounded" id="exampleInputEmail2"  placeholder="Enter email">
                                </div>

                                <div class="col-md-6 form-group mb-3">
                                    <label for="password">{{ __('Password') }}</label>
                                    <input id="password" type="password" class="form-control form-control-rounded @error('password') is-invalid @enderror" name="password" placeholder="Entrez votre mot de passe"  autocomplete="new-password">
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="password_confirmation">{{ __('Confirm Password') }}</label>
                                    <input id="password_confirmation" type="password" class="form-control form-control-rounded"
                                    name="password_confirmation" autocomplete="new-password"
                                    placeholder="Confirmez votre mot de passe">
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="cni">Cni</label>
                                    <input type="number" name="cni" class="form-control form-control-rounded" id="cni" placeholder="Entrez votre numéro de carte d'identité">
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="photo">Photo</label>
                                    <input name="photo" type="file" class="form-control form-control-rounded" id="photo" placeholder="Web address">
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="active">Statut</label>
                                    <select name="active" class="form-control form-control-rounded">
                                        <option value="0" >Inactive</option>
                                        <option value="1"> Active</option>

                                    </select>
                                </div>
                                <div class="col-md-6 form-group mb-3">
                                    <label for="role_id">Role</label>
                                    <select name="role_id" class="form-control form-control-rounded">
                                        @foreach ($roles as $role)
                                            <option value="{{ $role->id }}" >{{ $role->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12">
                                     <button class="btn btn-primary">Ajouter un administrateur</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
