<!DOCTYPE html>
<html>

<head>
    <title>SAMA KEYITTU JUDDU</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{ asset('css/login-page.css') }}">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous" />
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;400;500&display=swap" rel="stylesheet" />
</head>

<body>
    <div class="container hq-template">
        <div class="row">
            <div class="pic-block col-lg-7 col-md-12 col-sm-12 col-11">
                <div class="overlay">
                    <div class="welcome">
                        <h3></h3>
                    </div>
                </div>
            </div>
            <div class="login-block col-lg-5 col-md-12 col-sm-12 col-11">
                <section>
                    <div class="rt-container">
                        <div class="col-rt-12">
                            <div class="Scriptcontent">
                                <!-- Stylish Login Page Start -->
                                <form class="codehim-form" method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-title">
                                        <div class="user-icon gr-bg">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <h2>CONNEXION</h2>
                                    </div>
                                    <label for="email"><i class="fa fa-envelope"></i> Email:</label>
                                    <input type="email" id="email" class="cm-input" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Enter your email adress" />
                                    <label for="pass"><i class="fa fa-lock"></i> Password:</label>
                                    <input id="pass" type="password" class="cm-input" placeholder="Enter your password" name="password" required autocomplete="current-password" />
                                    <br />
                                    <br />
                                    <button type="submit" class="btn-login gr-bg">Login</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>

                <p>Suivez nous sur  nos réseaux sociaux </p>
                <div class="social-media">
                    <a href="https://www.facebook.com/Digital_Nisa-107314887611273" class="facebook"><i class="fab fa-facebook-f"></i></a>
                    <a href="https://twitter.com/DigitalNisa" class="twitter"><i class="fab fa-twitter"></i></a>
                    <a href="https://instagram.com/digital_nisa?igshid=1201tvkdfkce1" class="google"><i class="fab fa-instagram"></i></a>
                    <a href="https://www.linkedin.com/company/digital-nisa/" class="linkedin"><i class="fab fa-linkedin-in"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <a href="https://www.digitalnisa.com">
            <p>Copyright DIGITAL NISA 2021</p>
        </a>
    </div>

    <script src="https://kit.fontawesome.com/5118cb3e8d.js" crossorigin="anonymous"></script>
</body>

</html>