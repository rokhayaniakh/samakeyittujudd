@extends('layouts.master')

@section('page-css')
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection
@section('main-content')
@include('admin.includes.breadcrumb',[
        'title' => 'Les utilisateurs'])

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card text-left">
                    <div class="card-header text-right bg-transparent">
                        <a href="{{ route('admin.register') }}" type="button" class="btn btn-primary btn-md m-1">
                            <i class="i-Add text-white mr-2"></i> Ajouter un administrateur</a>
                    </div>
                    <div class="card-body">
                        <h4 class="card-title mb-3">Liste des utilisateurs</h4>

                        <div class="table-responsive">
                            <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Email</th>
                                        <th>Nom</th>
                                        <th>Statut</th>
                                        <th>Cni</th>
                                        <th>Photo</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($admins as $admin)
                                        <tr>
                                            <td>{{ $admin->email }}</td>
                                            <td>{{ $admin->firstname .' '. $admin->lastname }}</td>

                                            <td>
                                                @if ($admin->active)
                                                    <span class="badge badge-success">Active</span>
                                                @else
                                                <span class="badge badge-danger">Inactive</span>
                                                @endif
                                            </td>
                                            <td>{{ $admin->cni }}</td>
                                            <td>

                                                @if ($admin->getMedia()->first() !== null)
                                                    <img src="{{ asset( $admin->getMedia()->first()->getUrl()) }}" class=" rounded-circle avatar-sm-table m-auto"  alt="">
                                                @endif
                                            </td>
                                            <td>
                                                @if ($admin->active==0)
                                                    <a class="text-warning mr-2" href=""  onclick="event.preventDefault();
                                                    document.getElementById('active-post-{{ $admin->id }}').submit();"
                                                    >
                                                        <i class="nav-icon i-Restore-Window font-weight-bold"></i>Activer
                                                        <form id="active-post-{{ $admin->id }}" action="{{ route('admin.activation',[$admin->id]) }}" method="POST" style="display: none;">
                                                            @csrf @method('post')
                                                        </form>
                                                    </a>
                                                @else
                                                    <a class="text-warning mr-2" href=""  onclick="event.preventDefault();
                                                    document.getElementById('active-delete-{{ $admin->id }}').submit();"
                                                    >
                                                        <i class="nav-icon i-Close font-weight-bold"></i>Désactiver
                                                        <form id="active-delete-{{ $admin->id }}" action="{{ route('admin.deactivation',[$admin->id]) }}" method="POST" style="display: none;">
                                                            @csrf @method('delete')
                                                        </form>
                                                    </a>
                                                @endif

                                                <a href="{{ route("admin.edit",$admin) }}" class="text-success mr-2">
                                                    <i class="nav-icon i-Pen-2 font-weight-bold"></i> Editer
                                                </a>

                                                <a href="" class="text-danger mr-2"
                                                    onclick="event.preventDefault();
                                                    document.getElementById('delete-form-{{ $admin->id }}').submit();">
                                                    <i class="nav-icon i-Close-Window font-weight-bold"></i>Supprimer
                                                    <form id="delete-form-{{ $admin->id }}" action="{{ route('admin.delete',[$admin->id]) }}" method="POST" style="display: none;">
                                                        @csrf @method('delete')
                                                    </form>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>
@endsection

