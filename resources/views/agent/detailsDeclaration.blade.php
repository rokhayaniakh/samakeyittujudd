@extends('layouts.master')

@section('main-content')
@include('admin.includes.breadcrumb',[
'title' => 'Modification déclaration'])
<?php

use Illuminate\Support\Facades\DB;
?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    @if ($errors->count()>0)
                    @foreach ($errors->all() as $error)
                    <div class="alert alert-card alert-danger" role="alert">
                        <strong class="text-capitalize">Erreur!</strong>
                        {{ $error }}
                        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                    </div>
                    @endforeach
                    @endif
                    <div class="card-title mb-3">Détails déclaration </div>
                    <form method="POST" action="/admin/update-declaration-saving/{{ $declaration->id }}" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group row 2 ">
                            <div class="col-md-6 " id="prenom">
                                <label for="" class="col-form-label">Registre</label>
                                <input type="text" name="registre" class="form-control" value="{{$declaration->registre}}" disabled />
                            </div>
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Prénom</label>
                                <input type="text" name="prenom" class="form-control" value="{{$declaration->prenom}}" disabled />
                            </div>
                        </div>
                        <div class="form-group row 2 ">
                            <div class="col-md-6 " id="prenom">
                                <label for="" class="col-form-label">Date de Naissance</label>
                                <input type="text" name="date_naissance" class="form-control" value="{{$declaration->date_naissance}}" disabled />
                            </div>
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Heure de Naissance</label>
                                <input type="text" name="heure_naissance" class="form-control" value="{{$declaration->heure_naissance}}" disabled />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Nom</label>
                                <input type="text" name="nom" class="form-control" value="{{$declaration->nom}}" disabled />
                            </div>

                            <div class="col-md-6">
                                <label for="mairie" class="col-form-label">Genre</label>
                                <select name="genre" class="form-control " disabled>

                                    <option value="Homme" <?php if ($declaration->genre == 'Homme') {
                                                                echo "selected";
                                                            } ?>>Homme</option>
                                    <option value="Femme" <?php if ($declaration->genre == 'Femme') {
                                                                echo "selected";
                                                            } ?>>Femme</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class=" col-md-6 ">
                                <label for="" class="col-form-label">Region</label>
                                <select class="form-control" name="region" id="region" disabled>
                                    <?php
                                    $region1 = DB::table('ml_region')->where('id', $declaration->region)->first(); ?>
                                    <option value="{{ $region1->id}}"> Région actuelle :{{ $region1->nom }}</option>
                                    
                                
                                    <?php

                                    use App\Models\Region;

                                    $reg = Region::all(); ?>

                                    @foreach($reg as $dataReg)
                                    <option value="{{$dataReg->id}}">
                                        {{$dataReg->nom}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Departement</label>
                                <select class="form-control" name="departement" id="departement" disabled>
                                    <?php $departement1 = DB::table('ml_departement')->where('id', $declaration->departement)->first(); ?>
                                    <option value=" {{ $departement1->id}}"> Departement actuel :{{ $departement1->nom }}</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Commune</label>
                                <select class="form-control" name="commune" id="commune" disabled>
                                    <?php $commune1 = DB::table('ml_commune')->where('id', $declaration->commune)->first(); ?>
                                    <option value="{{ $commune1->id }}"> Commune actuelle : {{ $commune1->nom }} </option>
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label for="" class="col-form-label">Localité</label>
                                <select class="form-control" name="localite" id="localite" disabled>
                                    <?php $localite = DB::table('ml_localite')->where('id', $declaration->localite)->first(); ?>
                                    <option value="{{$localite->id}}"> Localité actuelle : {{$localite->nom}} </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Hopital </label>
                                <input type="text" name="hopital" class="form-control" value="{{$declaration->hopital}}" disabled />
                            </div>
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Prenom du père </label>
                                <input type="text" name="prenom_pere" class="form-control" value="{{$declaration->prenom_pere}}" disabled />
                            </div>
                        </div>
                        <div class="form-group row">

                            <div class="col-md-6">
                                <label for="" class="col-form-label">Nom du père </label>
                                <input type="text" name="nom_pere" class="form-control" value="{{$declaration->nom_pere}}" disabled />
                            </div>

                            <div class="col-md-6">
                                <label for="" class="col-form-label">Prénom de la mère </label>
                                <input type="text" name="prenom_mere" class="form-control" value="{{$declaration->prenom_mere}}" disabled />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Nom de la mère </label>
                                <input type="text" name="nom_mere" class="form-control" value="{{$declaration->nom_mere}}" disabled />
                            </div>
                            <div class="col-md-6">
                                <label for="cni_mere">Téphone Mère</label>
                                <input type="number" name="telephone_mere" class="form-control" value="{{$declaration->telephone_mere}}" disabled />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="statut" class="col-form-label">Statut</label>
                                <select name="statut" class="form-control" disabled>
                                    <option {{ $declaration->statut == 0 ? 'selected' : '' }} value="0">En traitement</option>
                                    <option {{ $declaration->statut == 1 ? 'selected' : '' }} value="1">Validé (Agent)</option>
                                    <option {{ $declaration->statut == 2 ? 'selected' : '' }} value="2">Validé (Officier d'état civil)</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="form-group row">
                            <div class="col-md-4 form-group mb-3">
                                <label for="cni_pere">Certificat d'accouchement</label>
                                <?php if (!empty($declaration->certificat)) { ?>
                                    <br>
                                    <img src="{{asset('assets/images/certificats/'.$declaration->certificat) }}" width="400" height="450">
                                <?php } else { ?>
                                    <div class="alert alert-primary" role="alert">
                                        Le certificat de naissance n'as pas enore été ajouté
                                    </div>
                                <?php }
                                ?>
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label for="cni_pere">CNI du père</label>
                                <?php if (!empty($declaration->cni_pere)) { ?>
                                    <br>
                                    <img src="{{asset('assets/images/certificats/'.$declaration->cni_pere) }}" width="400" height="450">
                                <?php } else { ?>

                                    <div class="alert alert-primary" role="alert">
                                        Le CNI du père n'as pas enore été ajouté
                                    </div>
                                <?php }
                                ?>
                            </div>
                            <div class="col-md-4 form-group mb-3">
                                <label for="cni_mere">CNI de la mère</label>

                                <?php if (!empty($declaration->cni_mere)) { ?>

                                    <br>
                                    <img src="{{asset('assets/images/certificats/'.$declaration->cni_mere) }}" width="400" height="450">

                                <?php } else { ?>

                                    <div class="alert alert-primary" role="alert">
                                        La CNI de la mère n'as pas enore été ajouté
                                    </div>
                                <?php }
                                ?>
                            </div>
                        </div>
                        <a href="/agent/declaration" class="btn btn-danger" data-dismiss="modal">retour à la liste</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('page-js')
<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<script src="{{asset('assets/js/datatables.script.js')}}"></script>
<script src="{{asset('assets/js/acat.js')}}"></script>
<script src="{{asset('assets/js/choix.js')}}"></script>
@endsection