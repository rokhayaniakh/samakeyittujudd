@extends('layouts.master')


@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
@include('admin.includes.breadcrumb',[
'title' => 'Déclaration'])
<?php

use Illuminate\Support\Facades\DB;
?>

<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form method="post" action="/admin/ajoutDeclaration" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
                @csrf
                <div class="card ">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Ajout d'une déclaration </h4>
                    </div>
                    <div class="card-body ">

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="genre" class="col-form-label">Genre</label>
                                <select name="genre" class="form-control " required>
                                    <option value="Homme">Homme</option>
                                    <option value="Femme">Femme</option>
                                </select>
                            </div>
                            <div class="col-md-6 ">
                                <label class="col-form-label">Téphone Mère</label>
                                <input type="number" name="telephone_mere" class="form-control {{ $errors->has('telephone_mere') ? 'is-invalid' : '' }}" value="{{ old('telephone_mere') }}">
                                {!! $errors->first('telephone_mere', '<div class="invalid-feedback">:message</div>') !!}
                            </div>
                        </div>
                        <div class="form-group row ">
                            <div class=" col-md-6 ">
                                <label for="" class="col-form-label">Region</label>
                                <select class="form-control" name="region" id="region" required>
                                    <option value="">--Sélectionner la région</option>
                                    <?php

                                    use App\Models\Region;

                                    $reg = Region::all(); ?>
                                    @foreach($reg as $dataReg)
                                    <option value="{{$dataReg->id}}" class="form-control {{ $errors->has('region') ? 'is-invalid' : '' }}" value="{{ old('region') }}">
                                        {{$dataReg->nom}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label for="" class="col-form-label">Departement</label>
                                <select class="form-control" name="departement" id="departement" required>
                                    <option value="" class="form-control {{ $errors->has('departement') ? 'is-invalid' : '' }}" value="{{ old('departement') }}">--Sélectionnez le département</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row ">

                            <div class="col-md-6">
                                <label for="" class="col-form-label">Commune</label>
                                <select class="form-control" name="commune" id="commune" required>
                                    <option value="" class="form-control {{ $errors->has('commune') ? 'is-invalid' : '' }}" value="{{ old('commune') }}">--Sélectionnez la commune</option>
                                </select>
                            </div>

                            <div class="col-md-6">
                                <label for="" class="col-form-label">Localité</label>
                                <select class="form-control" name="localite" id="localite" required>
                                    <option value="" class="form-control {{ $errors->has('localite') ? 'is-invalid' : '' }}" value="{{ old('localite') }}">--Choisissez la localité</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Date de Naissance</label>
                                <input type="date" name="date_naissance" class="form-control {{ $errors->has('date_naissance') ? 'is-invalid' : '' }}" value="{{ old('date_naissance') }}" required />
                            </div>
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Heure de Naissnce</label>
                                <input type="time" name="heure_naissance" class="form-control {{ $errors->has('date_naissance') ? 'is-invalid' : '' }}" value="{{ old('heure_naissance') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Hopital </label>
                                <input type="text" name="hopital" class="form-control {{ $errors->has('hopital') ? 'is-invalid' : '' }}" value="{{ old('hopital') }}" required />
                                {!! $errors->first('hopital', '<div class="invalid-feedback">:message</div>') !!}
                            </div>

                            <div class="col-md-6">
                                <label for="" class="col-form-label">Prenom du père </label>
                                <input type="text" name="prenom_pere" class="form-control {{ $errors->has('prenom_pere') ? 'is-invalid' : '' }}" value="{{ old('prenom_pere') }}" />
                                {!! $errors->first('prenom_pere', '<div class="invalid-feedback">:message</div>') !!}
                            </div>

                        </div>
                        <div class="form-group row">

                            <div class="col-md-6">
                                <label for="" class="col-form-label">Nom du père </label>
                                <input type="text" name="nom_pere" class="form-control {{ $errors->has('nom_pere') ? 'is-invalid' : '' }}" value="{{ old('nom_pere') }}" />
                                {!! $errors->first('nom_pere', '<div class="invalid-feedback">:message</div>') !!}
                            </div>

                            <div class="col-md-6">
                                <label for="" class="col-form-label">Prénom de la mère </label>
                                <input type="text" name="prenom_mere" class="form-control {{ $errors->has('prenom_mere') ? 'is-invalid' : '' }}" value="{{ old('prenom_mere') }}" required />
                                {!! $errors->first('prenom_pere', '<div class="invalid-feedback">:message</div>') !!}
                            </div>
                        </div>
                        <div class="form-group row">

                            <div class="col-md-6">
                                <label for="" class="col-form-label">Nom de la mère </label>
                                <input type="text" name="nom_mere" class="form-control {{ $errors->has('nom_mere') ? 'is-invalid' : '' }}" value="{{ old('nom_mere') }}" required />
                                {!! $errors->first('nom_mere', '<div class="invalid-feedback">:message</div>') !!}
                            </div>

                            <div class="col-md-6 form-group mb-3">
                                <label for="certificat">Certificat d'accouchement</label>
                                <input name="certificat" type="file" class="form-control form-control" id="certificat" placeholder="Certificat d'accouchement" value="{{ old('certificat') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 form-group mb-3">
                                <label for="cni_mere">CNI Mère</label>
                                <input name="cni_mere" type="file" class="form-control form-control" id="cni_mere" placeholder="CNI de la mère" value="{{ old('cni_mere') }}">
                            </div>
                            <div class="col-md-6 form-group mb-3">
                                <label for="cni_pere">CNI Père</label>
                                <input name="cni_pere" type="file" class="form-control form-control" id="cni_pere" placeholder="CNI du père" value="{{ old('cni_pere') }}">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ml-auto mr-auto">
                        <a class="btn btn-danger" data-dismiss="modal">Annuler</a>
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card text-left">
            <div class="card-header text-right bg-transparent">
                <a href="#adddeclaaration">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        <i class="i-Add text-white mr-2"></i> Ajouter une déclaration
                    </button>
                </a>
            </div>
            <div class="card-body">
                @if (session('success'))
                <div class="alert alert-success" role="alert">
                    <strong>{{session('success')}}</strong>
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                @endif
                <h4 class="card-title mb-3">Déclaration</h4>

                <div class="table-responsive">
                    <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Prenom etNom</th>
                                <th>Date de naissance</th>
                                <th>Lieu</th>
                                <th>Pére</th>
                                <th>Mére</th>
                                <th>Statut</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($declaration as $data)
                            <tr>
                                <td>{{ $data->id}}</td>
                                <td>{{ $data->prenom .' '. $data->nom }}</td>
                                <td>{{ date('d-m-Y', strtotime($data->created_at) )}}</td>
                                <td>
                                    <?php

                                    $localite = DB::table('ml_localite')->where('id', $data->localite)->first(); ?>
                                    {{ $localite->nom }}
                                </td>
                                <td>{{ $data->prenom_pere .' '. $data->nom_pere }}</td>
                                <td>{{ $data->prenom_mere .' '. $data->nom_mere }}</td>

                                <td>
                                    @if($data->statut==0)
                                    <span class="badge badge-warning">Incomplet</span>
                                    @endif

                                    @if($data->statut==1)
                                    <span class="badge badge-success">En Traitement</span>
                                    @endif

                                    @if($data->statut==2)
                                    <span class="badge badge-success">Validé</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="" class="text-warning mr-2" onclick="event.preventDefault();
                                            document.getElementById('details-form-{{ $data->id }}').submit();">
                                        <i class="icon-file-text"></i>Détails
                                        <form id="details-form-{{ $data->id }}" action="{{ url('admin/details-declaration/'.$data->id)}}" method="get" style="display: none;">
                                        </form>
                                    </a>
                                    <a href="" class="text-success mr-2" onclick="event.preventDefault();
                                            document.getElementById('modif-form-{{ $data->id }}').submit();">
                                        <i class="nav-icon i-Pen-2 font-weight-bold"></i>Modifier
                                        <form id="modif-form-{{ $data->id }}" action="{{ url('admin/modification-declaration/'.$data->id)}}" method="get" style="display: none;">
                                        </form>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')
@if(Session::has('errors'))
<script>
    $(document).ready(function() {
        $('#exampleModal').modal({
            show: true
        });
    })
</script>
@endif
<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<script src="{{asset('assets/js/datatables.script.js')}}"></script>
<script src="{{asset('assets/js/acat.js')}}"></script>
<script src="{{asset('assets/js/choix.js')}}"></script>

@endsection