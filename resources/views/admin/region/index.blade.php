@extends('layouts.master')

@section('page-css')

    <link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
    @include('admin.includes.breadcrumb',[
        'title' => 'Les régions'
        ])
    <div class="row">
        <div class="col-md-12 mb-4">
            <div class="card text-left">
                <div class="card-header text-right bg-transparent">
                    <a href="{{ route('region.create') }}" type="button" class="btn btn-primary btn-md m-1"><i class="i-Add text-white mr-2"></i> Ajouter une région</a>
                </div>
                <div class="card-body">
                    <h4 class="card-title mb-3">Liste des régions</h4>

                    <div class="table-responsive">
                        <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nom</th>
                                    <th>Nombre d'inscrits</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($regions as $region)


                                    @php
                                        $nbre= 0
                                    @endphp

                                    <div style="display: none">
                                        @foreach ($region->mairies as $mairie)
                                        {{ $nbre += $mairie->childrens()->count() }}
                                        @endforeach
                                    </div>

                                    <tr>
                                        <td>{{ $region->id }}</td>
                                        <td>{{ $region->name }}</td>
                                        <td>
                                            {{ $nbre }}
                                        </td>
                                        <td>
                                            <a href="{{ route('region.edit',$region) }}" class="text-success mr-2">
                                                <i class="nav-icon i-Pen-2 font-weight-bold"></i>Editer
                                            </a>
                                            <a href="{{ route('region.delete',$region) }}" class="text-danger mr-2">
                                                <i class="nav-icon i-Close-Window font-weight-bold"></i>Supprimer
                                            </a>
                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection


@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
