@extends('layouts.master')

@section('main-content')
    @include('admin.includes.breadcrumb',[
        'title' => 'Les mairies'])

    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    @if ($errors->count()>0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-card alert-danger" role="alert">
                                    <strong class="text-capitalize">Erreur!</strong>
                                        {{ $error }}
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                </div>
                            @endforeach

                        @endif
                    <div class="card-title mb-3">Editer une mairie</div>
                    <form action="{{ route('mairie.update',$mairie) }}" class="needs-validation" method="POST" novalidate>
                        @csrf
                        @method('put')
                        <div class="row">
                            <div class="col-md-12 form-group mb-3">
                                <label for="name">Nom de la mairie</label>
                                <input name="name" type="text" required class="form-control form-control-rounded "
                                id="name" value="{{ $mairie->name }}">
                                <div class="valid-feedback">
                                </div>
                            </div>
                            <div class="col-md-12 form-group mb-3">
                                <label for="picker1">Région</label>
                                <select name="region_id" class="form-control form-control-rounded">
                                    @foreach ($regions as $region)
                                        @if ($region->id == $mairie->region_id )
                                            <option selected value={{ $region->id }}>{{ $region->name }}</option>
                                        @else
                                            <option value={{ $region->id }}>{{ $region->name }}</option>
                                        @endif
                                    @endforeach

                                </select>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-primary">Modifier</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
