@extends('layouts.master')

@section('main-content')
    @include('admin.includes.breadcrumb',[
        'title' => 'Les mairies'])
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        @if ($errors->count()>0)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-card alert-danger" role="alert">
                                    <strong class="text-capitalize">Erreur!</strong>
                                        {{ $error }}
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                </div>
                            @endforeach

                        @endif
                        <div class="card-title mb-3">Ajouter une mairie</div>
                        <form action="{{ route('mairie.store') }}" class="needs-validation" method="POST" novalidate>
                            @csrf
                            <div class="row">
                                <div class="col-md-12 form-group mb-3">
                                    <label for="name">Nom de la mairie</label>
                                    <input name="name" type="text" required class="form-control form-control-rounded "
                                    id="name" placeholder="Entrez le nom de la mairie">
                                    <div class="valid-feedback">
                                    </div>
                                </div>
                                <div class="col-md-12 form-group mb-3">
                                    <label for="picker1">Région</label>
                                    <select name="region_id" class="form-control form-control-rounded">
                                        @foreach ($regions as $region)
                                            <option value={{ $region->id }}>{{ $region->name }}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-primary">Ajouter</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection
