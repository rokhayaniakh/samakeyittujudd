@extends('layouts.master')

@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
@include('admin.includes.breadcrumb',[
'title' => 'Déclaration'])
<?php

use Illuminate\Support\Facades\DB;
?>
<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card text-left">
            <div class="card-header text-right bg-transparent">
            </div>
            <div class="card-body">
                @if (session('success'))
                <div class="alert alert-success" role="alert">
                    <strong>{{session('success')}}</strong>
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                @endif
                <h4 class="card-title mb-3">Déclaration</h4>
                <div class="table-responsive">
                    <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <center><a class="btn btn-success" href="{{ route('export') }}"> <i class="icon-file-text"></i> Excel file</a></center>
                            </tr>
                            <tr>
                                <th>ID</th>
                                <th>Prenom etNom</th>
                                <th>Date et heure de naissance</th>
                                <th>Lieu</th>
                                <th>Pére</th>
                                <th>Mére</th>
                                <th>Statut</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($declaration as $data)
                            <tr>
                                <td>{{ $data->id}}</td>
                                <td>{{ $data->prenom .' '. $data->nom }}</td>
                                <td>{{ date('d-m-Y', strtotime($data->date_naissance)) .' à '. $data->heure_naissance}}</td>
                                <td>
                                    <?php

                                    $localite = DB::table('ml_commune')->where('id', $data->commune)->first(); ?>
                                    {{ $localite->nom }}
                                </td>
                                <td>{{ $data->prenom_pere .' '. $data->nom_pere }}</td>
                                <td>{{ $data->prenom_mere .' '. $data->nom_mere }}</td>

                                <td>
                                    @if($data->statut==0)
                                    <span class="badge badge-danger">Incomplet</span>
                                    @endif

                                    @if($data->statut==1)
                                    <span class="badge badge-primary">Reste à valider </span>
                                    @endif

                                    @if($data->statut==2)
                                    <span class="badge badge-success"> valider </span>
                                    @endif
                                </td>
                                <td>
                                    @if($data->statut==1)
                                    <a href="" class="text-success mr-2" onclick="event.preventDefault();
                                            document.getElementById('valid-form-{{ $data->id }}').submit();">
                                        <i class="icon-check"></i> Validé
                                        <form id="valid-form-{{ $data->id }}" action="{{ url('officier/valider/'.$data->id)}}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                            {{ method_field('PUT') }}
                                        </form>
                                    </a>
                                    @endif
                                    <a href="" class="text-warning mr-2" onclick="event.preventDefault();
                                            document.getElementById('details-form-{{ $data->id }}').submit();">
                                        <i class="nav-icon icon-plus"></i> Détails
                                        <form id="details-form-{{ $data->id }}" action="{{ url('officier/details-declaration/'.$data->id)}}" method="get" style="display: none;">
                                        </form>
                                    </a>
                                    @if($data->statut==2)
                                    <a href="" class="text-success mr-2" onclick="event.preventDefault();
                                            document.getElementById('modif-form-{{ $data->id }}').submit();">
                                        <i class="nav-icon i-Pen-2 font-weight-bold"></i>Modifier
                                        <form id="modif-form-{{ $data->id }}" action="{{ url('officier/modification-declaration/'.$data->id)}}" method="get" style="display: none;">
                                        </form>
                                    </a>
                                    @endif
                                    @if($data->statut==2)
                                    <a href="" class="text-success mr-2" onclick="event.preventDefault();
                                            document.getElementById('pdf-form-{{ $data->id }}').submit();">
                                        <i class="icon-download"></i>Extrait
                                        <form id="pdf-form-{{ $data->id }}" action="{{ url('officier/pdf/'.$data->id)}}" method="get" style="display: none;">
                                        </form>
                                    </a>
                                    @endif
                                    @if($data->statut==2)
                                    <a href="" class="text-success mr-2" onclick="event.preventDefault();
                                            document.getElementById('volet-form-{{ $data->id }}').submit();">
                                        <i class="icon-download"></i>Volet
                                        <form id="volet-form-{{ $data->id }}" action="{{ url('officier/volet/'.$data->id)}}" method="get" style="display: none;">
                                        </form>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-js')

<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<script src="{{asset('assets/js/datatables.script.js')}}"></script>
<script src="{{asset('assets/js/acat.js')}}"></script>
<script src="{{asset('assets/js/choix.js')}}"></script>

@endsection