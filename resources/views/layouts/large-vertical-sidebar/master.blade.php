<!-- ============ Large SIdebar Layout start ============= -->

<?php use Illuminate\Support\Facades\Auth; ?>



<div class="app-admin-wrap layout-sidebar-large clearfix">
    @include('layouts.large-vertical-sidebar.header')

    <!-- ============ end of header menu ============= -->

        @if(Auth::user()->profil==1)
             @include('layouts.large-vertical-sidebar.sidebar')
        @endif

        @if(Auth::user()->profil==2)
            @include('layouts.large-vertical-sidebar.sidebar')
        @endif

        @if(Auth::user()->profil==3)
            @include('layouts.large-vertical-sidebar.sidedeclarant')
        @endif

        @if(Auth::user()->profil==4)
            @include('layouts.large-vertical-sidebar.sideagent')
        @endif
        
        @if(Auth::user()->profil==5)
            @include('layouts.large-vertical-sidebar.sideofficier')
        @endif

    

    <!-- ============ end of left sidebar ============= -->

    <!-- ============ Body content start ============= -->
    <div class="main-content-wrap sidenav-open d-flex flex-column flex-grow-1">
        <div class="container">
            <div class="main-content">
                @yield('main-content')
            </div>
        </div>
        <div class="flex-grow-1"></div>
        @include('layouts.common.footer')
    </div>
    <!-- ============ Body content End ============= -->
</div>
<!--=============== End app-admin-wrap ================-->

<!-- ============ Search UI Start ============= -->
@include('layouts.common.search')
<!-- ============ Search UI End ============= -->




<!-- ============ Large Sidebar Layout End ============= -->