<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left">

            <li class="nav-item ">
                <a class="nav-item-hold" href="/officier/declaration">
                    <i class="nav-icon i-Baby"></i>
                    <span class="nav-text">Déclaration</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item ">
                <a class="nav-item-hold" href="/admin/cartographie">
                    <i class="nav-icon i-Map"></i>
                    <span class="nav-text">Région</span>
                </a>
                <div class="triangle"></div>
            </li>

        </ul>
    </div>
    <div class="sidebar-overlay"></div>
</div>
<!--=============== Left side End ================-->
<style>
    .layout-sidebar-large .sidebar-left.open {
        left: 0;
    }

    .layout-sidebar-large .sidebar-left {
        left: calc(-120px - 20px);
        z-index: 90;
        transition: left 0.24s ease-in-out;
    }

    .layout-sidebar-large .sidebar-left-secondary,
    .layout-sidebar-large .sidebar-left {
        position: fixed;
        top: 80px;
        height: calc(100vh - 80px);
        background: rgb(93, 17, 100);
        box-shadow: 0 4px 20px 1px rgb(0 0 0 / 6%), 0 1px 4px rgb(0 0 0 / 8%);
    }

    a {
        color: white;
        text-decoration: none;
        background-color: transparent;
    }

    .layout-sidebar-large .sidebar-left .navigation-left .nav-item .nav-item-hold .nav-icon,
    .layout-sidebar-large .sidebar-left .navigation-left .nav-item .nav-item-hold .feather {
        font-size: 32px;
        height: 32px;
        width: 32px;
        display: block;
        margin: 0 auto 6px;
        color: white;
    }

    .layout-sidebar-large .sidebar-left .navigation-left .nav-item .nav-item-hold {
        display: block;
        width: 100%;
        padding: 26px 0;
        color: white;
    }
</style>