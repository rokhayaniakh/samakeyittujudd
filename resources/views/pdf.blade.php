<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>{{ config('app.name', 'Laravel') }}</title>
  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>
  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
  <!-- <link rel="stylesheet" href="{{ asset('css/extrait.css') }}"> -->
  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body class="" style="background-color: #969696;">
  <?php

  use Illuminate\Support\Facades\DB;
  ?>

  <div class="wrapper ">
    <div class="main-panel">
      <!-- Navbar -->
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12 ">
              <center>
                <br><br><br>
                <u style="color: white;" style="margin-left: 300px">
                  <h1 style="text-align: center; color: white; text-transform: uppercase;">Aperçu du document </h1>
                </u>
                <button class="btn btn-primary" id="download" type="submit"> TELECHARGER </button>
                <br><br>
              </center>

              <div class="card " id="testpdf" style="border: none;">
                <div class="card-header card-header-primary">
                  <h4 class="card-title"></h4>
                </div>
                <div class="card-body">

                  <div class="row bloc1" style="width: 100%; height: 220px;">
                    <div class="block1_1 col-md-6" style="border: 1px solid black;  width: 50%; height: 220px;">

                      <p style="margin-left:5px;font-size:12px;">
                        <?php $region1 = DB::table('ml_region')->where('id', $declaration->region)->first(); ?>
                        <span style="font-size:15px;"> REGION DE {{ $region1->nom }}</span>
                      </p>
                      <hr style="width: 98%; margin-top:-12px;">
                      <?php $departement1 = DB::table('ml_departement')->where('id', $declaration->departement)->first(); ?>

                      <p style="margin-left:5px;font-size:12px;">
                        <span style="font-size:15px;"> DEPARTENMENT DE {{ $departement1->nom }}</span>
                      </p>
                      <hr style="width: 98%; margin-top:-12px;">
                      <p style="margin-left:5px;font-size:12px;">
                        <?php $commune1 = DB::table('ml_commune')->where('id', $declaration->commune)->first(); ?>

                        <span style="font-size:15px;">ARRONDISSEMENT DE {{ $commune1->nom }}</span>
                      </p>
                      <hr style="width: 98%; margin-top:-12px;">

                      <h6 style="text-align: center;">
                        <?php $commune1 = DB::table('ml_commune')->where('id', $declaration->commune)->first(); ?>

                        <p style="text-align: center;"> COLLECTIVITE LOCALE </p> <br>

                        <p style="text-align: center; font-size:10px; margin-top:-30px;">
                          (Commune ou Communauté Rurale)<br> DE
                        </p> <span text-transform:uppercase;>{{ $commune1->nom }}</span>
                      </h6>

                    </div>
                    <div class="block1_1 col-md-6" style="border: 1px solid black;  width: 50%; height: 220px;  ">
                      <p style="margin-left:5px;font-size:10px;text-align: center;">
                        REPUBLIQUE DU SENEGAL
                      </p>
                      <hr style="width: 8%; margin-top:-5px;">
                      <p style="text-align: center; font-size:7px; margin-top:-8px;">
                        UN PEUPLE-UN BUT-UNE FOI
                      </p>
                      <hr style="width: 8%; margin-top:-5px;">

                      <p style="text-align: center; font-size:30px;font-weight:bold">
                        ETAT CIVIL
                      </p>
                      <p style="text-align: center; font-size:10px;">
                        <?php $commune1 = DB::table('ml_commune')->where('id', $declaration->commune)->first(); ?>

                        CENTRE PRINCIPAL (1) <br>
                        DE <br>
                        <span style="text-transform:uppercase;font-size:15px;">{{ $commune1->nom }}</span>
                      </p>
                    </div>

                  </div>

                  <div class="row bloc2" style="width: 100%; height: 130px;">
                    <div class="block2_1 col-md-10" style="border: 1px solid black;  height: 130px;">
                      <h1 style="  text-align:center;font-size:25px;font-weight:bold">
                        EXTRAIT DU REGISTRE DES ACTES DE NAISSANCE
                      </h1>

                      <?php
                      $date_dec = $declaration->date_naissance;
                      $dat_naiss = strtotime($date_dec);
                      $jour = date('d', $dat_naiss);
                      $mois = date('m', $dat_naiss);
                      $annee = date('Y', $dat_naiss);
                      include_once("nombre_en_lettre.php");
                      echo "<p style='margin-left:5px;font-size:10px;text-transform:uppercase;'>" . "Pour l'année (2)" . " " . int2str($annee) . "..............................." . "</p>";
                      ?>
                      <p style="margin-left:5px;font-size:10px;">

                        N° dans le registre <span style="font-size:15px;">  
                        {{$declaration->registre}}</span>.............................................................
                      </p>
                    </div>
                    <div class="block2_2 col-md-2" style="border: 1px solid black;  height: 130px;  ">
                      <h2 style="font-size:10px;">

                        <?php
                        $date_dec = $declaration->date_naissance;
                        $dat_naiss = strtotime($date_dec);
                        $jour = date('d', $dat_naiss);
                        $mois = date('m', $dat_naiss);
                        $annee = date('Y', $dat_naiss);
                        echo "<p style='text-align: center;font-size:15px;'>" . " AN: ......" . " " . $annee . "" . "</p>";
                        ?>
                        <span style="margin-left:45px; font-size:15px;">...{{$declaration->registre}}....</span><br><br><br><br>

                        <p style="text-align: center;"> N° dans le Registre en chiffres</p>
                      </h2>
                    </div>

                  </div>

                  <div class="bloc3 row" style="border: 1px solid black; width: 100%; height: 400px;">
                    <!-- <p style="margin-left:5px;font-size:10px;"> -->
                    <!-- <div class="row"> -->
                    <div class="col-md-12">
                      <?php $commune1 = DB::table('ml_commune')->where('id', $declaration->commune)->first();
                      $date_dec = $declaration->date_naissance;
                      $dat_naiss = strtotime($date_dec);
                      $jour = date('d', $dat_naiss);
                      $mois = date('m', $dat_naiss);
                      $annee = date('Y', $dat_naiss);
                      if ($mois == 3) {
                        include_once("nombre_en_lettre.php");
                        echo "<p style='font-weight:bold;text-transform:uppercase;padding:20px; font-size:18px;'>" . "LE" . " " . int2str($jour) . "  " .  "MARS" . "  " . int2str($annee) . "</p>";
                      } else if ($mois == 1) {
                        echo "<p style='font-weight:bold;text-transform:uppercase;padding:20px;'>" . "LE" . " " . int2str($jour) . "  " .  "JANVIER" . "  " . int2str($annee) . "</p>";
                      } else if ($mois == 2) {
                        echo "<p style='font-weight:bold;text-transform:uppercase;padding:20px;'>" . "LE" . " " . int2str($jour) . "  " .  "FEVRIER" . "  " . int2str($annee) . "</p>";
                      } else if ($mois == 4) {
                        echo "<p style='font-weight:bold;text-transform:uppercase;padding:20px;'>" . "LE" . " " . int2str($jour) . "  " .  "AVRIL" . "  " . int2str($annee) . "</p>";
                      } else if ($mois == 5) {
                        echo "<p style='font-weight:bold;text-transform:uppercase;padding:20px;'>" . "LE" . " " . int2str($jour) . "  " .  "MAI" . "  " . int2str($annee) . "</p>";
                      } else if ($mois == 6) {
                        echo "<p style='font-weight:bold;text-transform:uppercase;padding:20px;'>" . "LE" . " " . int2str($jour) . "  " .  "JUIN" . "  " . int2str($annee) . "</p>";
                      } else if ($mois == 7) {
                        echo "<p style='font-weight:bold;text-transform:uppercase;padding:20px;'>" . "LE" . " " . int2str($jour) . "  " .  "JUILLET" . "  " . int2str($annee) . "</p>";
                      } else if ($mois == 8) {
                        echo "<p style='font-weight:bold;text-transform:uppercase;padding:20px;'>" . "LE" . " " . int2str($jour) . "  " .  "AOUT" . "  " . int2str($annee) . "</p>";
                      } else if ($mois == 9) {
                        echo "<p style='font-weight:bold;text-transform:uppercase;padding:20px;'>" . "LE" . " " . int2str($jour) . "  " .  "SEPTEMBRE" . "  " . int2str($annee) . "</p>";
                      } else if ($mois == 10) {
                        echo "<p style='font-weight:bold;text-transform:uppercase;padding:20px;'>" . "LE" . " " . int2str($jour) . "  " .  "OCTOBRE" . "  " . int2str($annee) . "</p>";
                      } else if ($mois == 11) {
                        echo "<p style='font-weight:bold;text-transform:uppercase;padding:20px;'>" . "LE" . " " . int2str($jour) . "  " .  "NOVEMBRE" . "  " . int2str($annee) . "</p>";
                      } else if ($mois == 12) {
                        echo "<p style='font-weight:bold;text-transform:uppercase;padding:20px;'>" . "LE" . " " . int2str($jour) . "  " .  "DECEMBRE" . "  " . int2str($annee) . "</p>";
                      }
                      ?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6" style="margin-top:-340px;">
                      <?php $commune1 = DB::table('ml_commune')->where('id', $declaration->commune)->first();
                      $date_dec = $declaration->heure_naissance;
                      $heure_naiss = strtotime($date_dec);
                      $heure = date('H', $heure_naiss);
                      $minutes = date('i', $heure_naiss);
                      echo "<p style='padding:-20px; font-size:18px;'>" . "A........." . " " . $heure . "heures" . "......... " . $minutes . "minutes.........................." . "</p>";
                      ?>
                    </div>
                    <div class="col-md-6" style="margin-top:-340px;margin-left:680px;">
                      est né(e) à ...................... <span>{{$commune1->nom}}</span>................ <br>
                      <P style="margin-left:145px;">LIEU DE NAISSANCE</P>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6 " style="margin-top:-310px;">
                      <p style="margin-left:170px;">HEURE</p>
                    </div>
                  </div>

                  <div class="row">
                    <p class="col-md-8" style="margin-top:-275px;">Un enfant de sexe <span style="text-transform: uppercase;">{{$declaration->genre}} (4)</span> </p>
                  </div>
                  <div class="row">
                    <div class="col-md-6" style="margin-top:-230px;margin-left:100px;">
                      <span style="text-transform:uppercase;">........................{{$declaration->prenom}}...........................</span><br>
                      <P style="margin-left:72px;"> PRENOM(S)</P>
                    </div>
                    <div class="col-md-6" style="margin-top:-230px;margin-left:800px;">
                      <span style="text-transform:uppercase;">.........................{{$declaration->nom}}............................</span><br>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6 " style="margin-top:-200px;margin-left:680px;">
                      <p style="margin-left:150px;">NOM DE FAMILLE</p>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-8" style="margin-top:-180px;margin-left:100px;">
                      <p style="margin-left:-100px;">de </p>........................<span style="text-transform:uppercase;">{{$declaration->prenom_pere}}</span>....................... <br>
                      <P style="margin-left:50px;"> PRENOM DU PERE</P>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-8" style="margin-top:-135px;margin-left:100px;">
                      <p style="margin-left:-100px; margin-top:20px;"> et de </p>...........................<span style="text-transform:uppercase;">{{$declaration->prenom_mere}}</span>............................
                      <P style="margin-left:40px;"> PRENOM DE LA MERE</P>
                    </div>
                    <div class="col-md-6" style="margin-top:-90px;margin-left:800px;">
                      <span style="text-transform:uppercase;">.........................{{$declaration->nom_mere}}............................</span>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 " style="margin-top:-60px;margin-left:680px;">
                      <p style="margin-left:150px;">NOM DE FAMILLE DE LA MERE</p>
                    </div>
                  </div>
              
                    <p style="margin-top:-20px; height:5px;">Pays de naissance pour les naissances à l'etranger (3)....................................................................</p>

                  <div class="bloc4 row" style="border: 1px solid black; width: 100%; height: 180px;margin-top: -1px;">
                    <div class="block4_1 col-md-2" style="border: 1px solid black;  width: 10%; height: 180px;">
                      <p style="text-align: center;font-size:12px;margin-top:65px;" class="rotate">JUGEMENT D'AUTORISATION D'INSCRIPTION</p>
                    </div>
                    <div class="block4_2 col-md-8" style="border: 1px solid black;  width: 70%; height: 180px;">
                      <p style="margin-left:5px;font-size:10px; padding:15px;">
                        Delivrer par le juge de ..............................................................................................................................................................................
                      </p>
                      <p style="margin-left:5px;font-size:10px;">
                        Le....................................................................................................................................................Année....................................................................
                      </p>
                      <p style="margin-left:5px;font-size:10px;">
                        Sous le numero ..................................................................................................................................................
                      </p>
                      <p style="margin-left:5px;font-size:10px;">
                        Inscrit le .......................................................................................................................................................Sur le registre des Actes des Naissances de l'année........................
                      </p>
                    </div>
                    <div class="block4_3 col-md-2" style="border: 1px solid black;  width: 20%; height: 180px;">
                      <h2 style="margin-left:5px;font-size:10px;margin-top:30px;">
                        AN......
                      </h2>
                      <h2 style="margin-left:5px;font-size:10px;margin-top:30px;">
                        AN......
                      </h2>
                      <h2 style="margin-left:5px;font-size:10px;margin-top:30px;">
                        AN......
                      </h2>
                    </div>
                  </div>

                  <div class="bloc5 row" style="border: 1px solid black; width: 100%; height: 250px; ">
                    <div class="col-md-4">
                      <p style="margin-left:5px;font-size:12px;font-weight:bold; padding-top:20px;">
                        <?php $commune1 = DB::table('ml_commune')->where('id', $declaration->commune)->first(); ?>
                        EXTRAIT DELIVRER PAR LE CENTRE... DE..... <span>{{ $commune1->nom}} </span>
                      </p>
                    </div>
                    <div class="col-md-8">
                      <p style="font-size:12px;font-weight:bold;padding-top:20px;margin-left:400px;">
                        POUR EXTRAIT CERTIFIE CONFORME

                      </p><br>
                      <p style="font-size:10px;margin-left:400px;">
                        À....... <span>{{ $commune1->nom}}.......................................Le {{ $declaration->delivrance}}.................

                      </p>
                      <p style="font-size:10px;margin-left:400px;">
                        l'officier d'Etat civil sousigné
                      </p>
                      <p style="font-size:10px;margin-left:400px;">
                        <?php $user = DB::table('users')->where('id', $declaration->delivrer_par)->first(); ?>
                        Prénom et Nom <span style="text-transform: uppercase;">{{$user->prenom}} {{$user->name}}</span>.........................
                      </p>
                    </div>
                    <div class="row" style="height: 20px; width: 100%;margin-left:5px;">
                      <span>(1) (2) (3) Notes et mentions marginales au verso</span>
                    </div>

                  </div>
                  <div class="row" style="height: 48px;border: 1px solid black; width: 100%;">

                  </div>
                </div>
              </div>
            </div>
          </div>
        
          <style>
            .input1 {
              padding: 6px 5px;
              margin: 8px 200px;
              box-sizing: border-box;
              border: none;
              border-bottom: 1px solid black;
              border-right: 1px solid black;
              border-left: 1px solid black;
            }

            input[type=text],
            textarea {
              padding: 12px 0px;
              margin: 8px 0;
              box-sizing: border-box;
              border: none;
              border-bottom: 1px solid black;
              border-right: 1px solid black;
              border-left: 1px solid black;

            }

            span {
              font-weight: bold;
            }

            .rotate {

              transform: rotate(-90deg);


              /* Legacy vendor prefixes that you probably don't need... */

              /* Safari */
              -webkit-transform: rotate(-90deg);

              /* Firefox */
              -moz-transform: rotate(-90deg);

              /* IE */
              -ms-transform: rotate(-90deg);

              /* Opera */
              -o-transform: rotate(-90deg);

              /* Internet Explorer */
              filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);

            }
          </style>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.js"></script>
          <script>
            window.onload = function() {
              document.getElementById("download")
                .addEventListener("click", () => {
                  const invoice = this.document.getElementById("testpdf");
                  console.log(invoice);
                  console.log(window);
                  var opt = {
                    margin: 1,
                    filename: 'docskj.pdf',
                    image: {
                      type: 'jpeg',
                      quality: 0.98
                    },
                    html2canvas: {
                      scale: 3,
                      dpi: 300,
                      letterRendering: true
                    },
                    jsPDF: {
                      unit: 'mm',
                      format: 'a3',
                      orientation: 'portrait'
                    },
                  };
                  html2pdf().from(invoice).set(opt).save();
                })
            }
            //alert("fsddfsdf");
          </script>
          @yield('scripts')


        </div>
      </div>
    </div>
</body>

</html>