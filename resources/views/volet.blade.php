<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Laravel') }}</title>
  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>
  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body class="" style="background-color: #969696;">
  <?php

  use Illuminate\Support\Facades\DB;
  ?>

  <center>
    <br><br><br>
    <u style="color: white;">
      <h1 style="text-align: center; color: white; text-transform: uppercase;">Aperçu du document </h1>
    </u>
    <button class="btn btn-primary" id="download" type="submit"> TELECHARGER </button>
    <br><br>
  </center>

  <div class="card " id="testpdf" style="border: none;">
    <div id="container">
      <div class="row">
        <div class="col-md-6" style="margin-left: 20px;margin-top:30px;"><span>{{$declaration->delivrance}}</span></div>
      </div>
      <div class="block1_1 col-md-12" style="border: 1px solid black; height:500px;margin-top:25px; padding:20px;width:100%;">
        <h1 style="margin-left:5px;font-size:20px;font-weight:bold;">
          REPUBLIQUE DU SENEGAL
        </h1>
        <p style="margin-left:5px;font-size:10px;margin-top:30px;">
          <?php $region1 = DB::table('ml_region')->where('id', $declaration->region)->first(); ?>

          REGION: <span>{{ $region1->nom }}</span>.....................................................................................................................................................................................................................................
        </p>
        <p style="margin-left:5px;font-size:10px;">
          <?php $departement1 = DB::table('ml_departement')->where('id', $declaration->departement)->first(); ?>
          DEPARTENMENT: <span>{{ $departement1->nom }}</span>...........................................................................................................................................................................................................................
        </p>
        <p style="margin-left:5px;font-size:10px;">
          <?php $commune1 = DB::table('ml_commune')->where('id', $declaration->commune)->first(); ?>
          ARRONDISSEMENT: <span>{{ $commune1->nom }}</span>.............................................................................................................................................................................................
        </p>
        <p style="margin-left:5px;font-size:10px;">
          <?php $commune1 = DB::table('ml_commune')->where('id', $declaration->commune)->first(); ?>

          COMMUNE/COMMUNAUTE RURALE: <span>{{ $commune1->nom }}</span>...................................................................................................................................................................................
        </p>

        <h4 style="text-align: center; font-weight:bold;">
          VOLET D'ACTE D'ETAT CIVIL
        </h4>
        <p style="margin-left:5px;font-size:15px;">
          N° dans le registre: <span>{{$declaration->registre}}</span>.................................................................................................................................
        </p>
        <p style="margin-left:5px;font-size:15px;">
          Nom: <span>{{$declaration->nom}}</span>..................................................................................................................................................................
        </p>
        <p style="margin-left:5px;font-size:15px;">
          Prénom: <span>{{$declaration->prenom}}</span>...............................................................................................................................................................
        </p>
        <p style="margin-left:5px;font-size:15px;">
          Date de naissance: <span>{{$declaration->date_naissance}}</span>..............................................................................................................................
        </p>
      </div>
    </div>
  </div>
  <style>
    span {
      font-weight: bold;
    }
  </style>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.js"></script>
  <script>
    window.onload = function() {
      document.getElementById("download")
        .addEventListener("click", () => {
          const invoice = this.document.getElementById("testpdf");
          console.log(invoice);
          console.log(window);
          var opt = {
            margin: 1,
            filename: 'volet.pdf',
            image: {
              type: 'jpeg',
              quality: 0.98
            },
            html2canvas: {
              scale: 3,
              dpi: 300,
              letterRendering: true
            },
            jsPDF: {
              unit: 'mm',
              format: 'a4',
              orientation: 'portrait'
            },
          };
          html2pdf().from(invoice).set(opt).save();
        })
    }
    //alert("fsddfsdf");
  </script>
  @yield('scripts')
</body>

</html>