<?php

namespace App\Exports;

use App\Models\Declaration;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;


class DeclarationAgentExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function query()
    {
        return Declaration::query()->select(
            'id',
            'registre',
            'nom',
            'prenom',
            'genre',
            'hopital',
            'nom_mere',
            'prenom_mere',
            'nom_pere',
            'prenom_pere',
            'date_naissance',
            'heure_naissance',
            'telephone_mere',
        )->where('statut',1);
    }
    public function headings(): array
    {
        return [
            'id',
            'Régistre',
            'Nom',
            'Prénom',
            'Genre',
            'Distict',
            'Nom de la Mère',
            'Prénom de la Mère',
            'Nom du Père',
            'Prénom du Père',
            'Date de Naissance',
            'Heure de Naissance',
            'Numéro de la mére'
        ];
    }
}
