<?php

namespace App\Exports;

use App\Models\Declaration;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DeclarationExport implements FromQuery, WithHeadings

{
    use Exportable;

    public function query()
    {
        return Declaration::query()->select(
            'id',
            'registre',
            'nom',
            'prenom',
            'genre',
            'hopital',
            'nom_mere',
            'prenom_mere',
            'nom_pere',
            'prenom_pere',
            'date_naissance',
            'heure_naissance',
            'delivrance',
            'telephone_mere',
        )->where('statut',2);
    }
    public function headings(): array
    {
        return [
            'id',
            'Régistre',
            'Nom',
            'Prénom',
            'Genre',
            'Distict',
            'Nom de la Mère',
            'Prénom de la Mère',
            'Nom du Père',
            'Prénom du Père',
            'Date de Naissance',
            'Heure de Naissance',
            'Date de Délivrance',
            'Numéro de la mére'
        ];
    }
}
