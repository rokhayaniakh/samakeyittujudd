<?php

namespace App\Http\Controllers;

use App\Models\Declaration;
use App\Models\Mailer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Exports\DeclarationExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DeclarationAgentExport;
use App\Helpers\Sms;

class DeclarationController extends Controller
{
    public function index()
    {
        $user_connect = Auth::user()->commune;
        $declaration = DB::table('declarations')->where('commune', $user_connect)
            ->get();
        if (Auth::user()->profil == '1' or Auth::user()->profil == '2') {

            return view('admin.declaration.declaration', compact('declaration'));
        } elseif (Auth::user()->profil == '3') {

            return view('declarant.declaration', compact('declaration'));
        } elseif (Auth::user()->profil == '4') {

            return view('agent.declaration', compact('declaration'));
        } elseif (Auth::user()->profil == '5') {

            return view('officier.declaration', compact('declaration'));
        }
    }

    public function store(Request $request)
    {
        $declaration = new Declaration;
        $request->validate([

            'hopital' => 'bail|required|between:5,50|alpha',
            'prenom_pere' => 'bail|required|between:2,50|alpha',
            'nom_pere' => 'bail|required|between:2,50|alpha',
            'prenom_mere' => 'bail|required|between:2,50|alpha',
            'nom_mere' => 'bail|required|between:2,50|alpha',
            'telephone_mere' => 'bail|required|digits:9|numeric',

        ]);
        $declaration->genre = $request->input('genre');
        $declaration->region = $request->input('region');
        $declaration->departement = $request->input('departement');
        $declaration->commune = $request->input('commune');
        $declaration->localite = $request->input('localite');
        $declaration->hopital = $request->input('hopital');
        $declaration->date_naissance = $request->input('date_naissance');
        $declaration->heure_naissance = $request->input('heure_naissance');
        $declaration->prenom_pere = $request->input('prenom_pere');
        $declaration->nom_pere = $request->input('nom_pere');
        $declaration->prenom_mere = $request->input('prenom_mere');
        $declaration->nom_mere = $request->input('nom_mere');
        $declaration->telephone_mere = $request->input('telephone_mere');

        if (!empty($request->file('certificat'))) {

            $data = $request->input('certificat');
            $file = $request->file('certificat');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename = $imageName = time() . '.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);

            $declaration->certificat = $filename;
        }
        if (!empty($request->file('cni_pere'))) {

            $data = $request->input('cni_pere');
            $file = $request->file('cni_pere');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename = $imageName = time() . '.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);

            $declaration->cni_pere = $filename;
        }
        if (!empty($request->file('cni_mere'))) {

            $data = $request->input('cni_mere');
            $file = $request->file('cni_mere');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename = $imageName = time() . '.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);
            $declaration->cni_mere = $filename;
        }
        //try {
        $declaration->save();
        // return redirect('/admin/utilisateur')->with('success','Utilisateur ajouté avec succès');
        //} catch (\Exception $e) {

        // return redirect('/admin/utilisateur')->with('status','Utilisateur non ajouté .Vérifiez si vous avez bien rentré les données.');

        //}

        return redirect('/admin/declaration')->with('success', 'Enfant ajouté avec succés');
    }

    public function storeDeclarant(Request $request)
    {
        $declaration = new Declaration;
        $request->validate([

            'hopital' => 'bail|required|between:5,50|alpha',
            'prenom_pere' => 'bail|required|between:2,50|alpha',
            'nom_pere' => 'bail|required|between:2,50|alpha',
            'prenom_mere' => 'bail|required|between:2,50|alpha',
            'nom_mere' => 'bail|required|between:2,50|alpha',
            'telephone_mere' => 'bail|required|digits:9|numeric',

        ]);
        $declaration->genre = $request->input('genre');
        $declaration->region = $request->input('region');
        $declaration->departement = $request->input('departement');
        $declaration->commune = $request->input('commune');
        $declaration->localite = $request->input('localite');
        $declaration->hopital = $request->input('hopital');
        $declaration->date_naissance = $request->input('date_naissance');
        $declaration->heure_naissance = $request->input('heure_naissance');
        $declaration->prenom_pere = $request->input('prenom_pere');
        $declaration->nom_pere = $request->input('nom_pere');
        $declaration->prenom_mere = $request->input('prenom_mere');
        $declaration->nom_mere = $request->input('nom_mere');
        $declaration->telephone_mere = $request->input('telephone_mere');

        if (!empty($request->file('certificat'))) {
            $data = $request->input('certificat');
            $file = $request->file('certificat');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename = $imageName = time() . '.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);
            $declaration->certificat = $filename;
        }
        if (!empty($request->file('cni_pere'))) {

            $data = $request->input('cni_pere');
            $file = $request->file('cni_pere');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename = $imageName = time() . '.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);

            $declaration->cni_pere = $filename;
        }
        if (!empty($request->file('cni_mere'))) {

            $data = $request->input('cni_mere');
            $file = $request->file('cni_mere');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename = $imageName = time() . '.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);
            $declaration->cni_mere = $filename;
        }
        $declaration->save();

        //   $emails=DB::select('select email from users where  profil=4 AND  departement="'.$request->input('departement').'"');
        //   $lemessage='Bonjour vous avez la déclaration de "'.$request->input('prenom_mere').'" '.  $request->input('nom') .'" ('.  $request->input('cni_mere') .'" ) à traiter.'
        // // foreach ($emails as $data) {
        //      Mail::send('emails.contact',
        //      array(
        //          'objet' => "Déclaration à remplir",
        //          'msg' => $lemessage,
        //      ), function($message) use ($request)
        //        {
        //           $message->from('contact@ehodcorp.com');
        //           $message->subject($request->objet);
        //           $emails=DB::select('select email from annuaire where domaine="'.$request->input('domaine').'"');
        //           foreach ($emails as $data) {
        //             $message->to($data->email);
        //           }
        //        });
        // $mail=new Mailer;
        // $mail->objet="Déclaration à remplir";
        // $mail->message=$lemessage;
        // $mail->save();


        return redirect('/declarant/declaration')->with('success', 'Efant ajouté avec succès');
    }
    public function update(Request $request, $id)
    {
        $declaration = Declaration::findOrFail($id);
        $localisation = DB::select('select r.id as id_r,r.nom as nom_r,d.id as id_d,d.nom as nom_d,c.id as id_c,c.nom as nom_c,l.id as id_l,l.nom as nom_l from declarations dp ,ml_region r,ml_departement d ,ml_localite l ,ml_commune c WHERE 1  AND dp.region=r.id AND dp.departement=d.id AND dp.localite=l.id AND dp.commune=c.id');
        if (Auth::user()->profil == '1' or Auth::user()->profil == '2') {

            return view('admin.declaration.updateDeclaration', compact('declaration', 'localisation'));
        } elseif (Auth::user()->profil == '3') {

            return view('declarant.updateDeclaration', compact('declaration', 'localisation'));
        } elseif (Auth::user()->profil == '4') {

            return view('agent.updateDeclaration', compact('declaration',  'localisation'));
        } elseif (Auth::user()->profil == '5') {

            return view('officier.updateDeclaration', compact('declaration', 'localisation'));
        }
    }

    public function updateSaving(Request $request, $id)
    {
        $declaration = Declaration::find($id);
        $request->validate([

            'hopital' => 'bail|required|between:5,50|alpha',
            'prenom_pere' => 'bail|required|between:2,50|alpha',
            'nom_pere' => 'bail|required|between:2,50|alpha',
            'prenom_mere' => 'bail|required|between:2,50|alpha',
            'nom_mere' => 'bail|required|between:2,50|alpha',
            'telephone_mere' => 'bail|required|digits:9|numeric',

        ]);
        $declaration->registre = $request->input('registre');
        $declaration->prenom = $request->input('prenom');
        $declaration->nom = $request->input('nom');
        $declaration->genre = $request->input('genre');
        $declaration->region = $request->input('region');
        $declaration->departement = $request->input('departement');
        $declaration->commune = $request->input('commune');
        $declaration->localite = $request->input('localite');
        $declaration->hopital = $request->input('hopital');
        $declaration->prenom_pere = $request->input('prenom_pere');
        $declaration->nom_pere = $request->input('nom_pere');
        $declaration->prenom_mere = $request->input('prenom_mere');
        $declaration->nom_mere = $request->input('nom_mere');
        $declaration->date_naissance = $request->input('date_naissance');
        $declaration->heure_naissance = $request->input('heure_naissance');
        $declaration->telephone_mere = $request->input('telephone_mere');
        $declaration->statut = $request->input('statut');
        // $declaration->district = $request->input('district');
        if (!empty($request->file('certificat'))) {

            $data = $request->input('certificat');
            $file = $request->file('certificat');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename = $imageName = time() . '.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);

            $declaration->certificat = $filename;
        }
        if (!empty($request->file('cni_pere'))) {

            $data = $request->input('cni_pere');
            $file = $request->file('cni_pere');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename = $imageName = time() . '.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);
            $declaration->cni_pere = $filename;
        }
        if (!empty($request->file('cni_mere'))) {

            $data = $request->input('cni_mere');
            $file = $request->file('cni_mere');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename = $imageName = time() . '.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);
            $declaration->cni_mere = $filename;
        }

        $declaration->update();

        return redirect('/admin/declaration')->with('success', 'Déclaration mise à jour  avec succès');
    }
    public function updateSavingDeclarant(Request $request, $id)
    {
        $declaration = Declaration::find($id);
        $request->validate([

            'hopital' => 'bail|required|between:5,50|alpha',
            'prenom_pere' => 'bail|required|between:2,50|alpha',
            'nom_pere' => 'bail|required|between:2,50|alpha',
            'prenom_mere' => 'bail|required|between:2,50|alpha',
            'nom_mere' => 'bail|required|between:2,50|alpha',
            'telephone_mere' => 'bail|required|digits:9|numeric',

        ]);
        $declaration->genre = $request->input('genre');
        $declaration->region = $request->input('region');
        $declaration->departement = $request->input('departement');
        $declaration->commune = $request->input('commune');
        $declaration->localite = $request->input('localite');
        $declaration->hopital = $request->input('hopital');
        $declaration->prenom_pere = $request->input('prenom_pere');
        $declaration->nom_pere = $request->input('nom_pere');
        $declaration->prenom_mere = $request->input('prenom_mere');
        $declaration->nom_mere = $request->input('nom_mere');
        $declaration->date_naissance = $request->input('date_naissance');
        $declaration->heure_naissance = $request->input('heure_naissance');
        $declaration->telephone_mere = $request->input('telephone_mere');

        if (!empty($request->file('certificat'))) {

            $data = $request->input('certificat');
            $file = $request->file('certificat');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename = $imageName = time() . '.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);

            $declaration->certificat = $filename;
        }
        if (!empty($request->file('cni_pere'))) {

            $data = $request->input('cni_pere');
            $file = $request->file('cni_pere');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename = $imageName = time() . '.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);
            $declaration->cni_pere = $filename;
        }
        if (!empty($request->file('cni_mere'))) {

            $data = $request->input('cni_mere');
            $file = $request->file('cni_mere');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename = $imageName = time() . '.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);
            $declaration->cni_mere = $filename;
        }
        $declaration->update();
        return redirect('/declarant/declaration')->with('success', 'Déclaration mise à jour  avec succès');
    }

    public function updateSavingAgent(Request $request, $id)
    {
        $request->validate([

            'nom' => 'bail|required|between:2,50|alpha',
            'prenom' => 'bail|required|between:2,50|alpha',

        ]);
        $declaration = Declaration::find($id);
        $id_dec = $declaration->id;
        $date_dec = $declaration->date_naissance;
        $dat_naiss = strtotime($date_dec);
        $new_dat = date('Ymd', $dat_naiss);
        $declaration->registre = $id_dec . '' . $new_dat;
        $declaration->prenom = $request->input('prenom');
        $declaration->nom = $request->input('nom');
        $declaration->statut = 1;
        $declaration->agent_responsable = Auth::user()->id;
        $declaration->update();
        return redirect('/agent/declaration')->with('success', 'Déclaration mise à jour  avec succès');
    }

    public function updateSavingOfficier(Request $request, $id)
    {
        $declaration = Declaration::find($id);
        $request->validate([

            'delivrance' => 'required',
           
        ]);
        $declaration->registre = $request->input('registre');
        $declaration->prenom = $request->input('prenom');
        $declaration->nom = $request->input('nom');
        $declaration->genre = $request->input('genre');
        $declaration->region = $request->input('region');
        $declaration->departement = $request->input('departement');
        $declaration->commune = $request->input('commune');
        $declaration->localite = $request->input('localite');
        $declaration->hopital = $request->input('hopital');
        $declaration->prenom_pere = $request->input('prenom_pere');
        $declaration->nom_pere = $request->input('nom_pere');
        $declaration->prenom_mere = $request->input('prenom_mere');
        $declaration->nom_mere = $request->input('nom_mere');
        $declaration->cni_mere = $request->input('cni_mere');
        $declaration->cni_pere = $request->input('cni_pere');
        $declaration->telephone_mere = $request->input('telephone_mere');
        $declaration->statut = $request->input('statut');
        $declaration->date_naissance = $request->input('date_naissance');
        $declaration->heure_naissance = $request->input('heure_naissance');
        $declaration->delivrance = $request->input('delivrance');

        if (!empty($request->file('certificat'))) {
            $data = $request->input('certificat');
            $file = $request->file('certificat');
            $destinationPath = 'assets/images/certificats/';
            //$originalFile = $file->getClientOriginalName();
            $filename = $imageName = time() . '.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $filename);
            $declaration->certificat = $filename;
        }
        $declaration->update();
        return redirect('/officier/declaration')->with('success', 'Déclaration mise à jour  avec succès');
    }

    public function details(Request $request, $id)
    {
        $declaration = Declaration::findOrFail($id);
        $localisation = DB::select('select r.id as id_r,r.nom as nom_r, u.id as id_u,u.username as username_u,d.id as id_d,d.nom as nom_d,c.id as id_c,c.nom as nom_c,l.id as id_l,l.nom as nom_l from declarations dp ,ml_region r,ml_departement d ,ml_localite l ,ml_commune c , users u WHERE 1  AND dp.region=r.id AND dp.departement=d.id AND dp.localite=l.id AND dp.commune=c.id AND dp.delivrer_par=u.id');

        if (Auth::user()->profil == '1' or Auth::user()->profil == '2') {

            return view('admin.declaration.detailsDeclaration', compact('declaration', 'localisation'));
        } elseif (Auth::user()->profil == '3') {

            return view('declarant.detailsDeclaration', compact('declaration', 'localisation'));
        } elseif (Auth::user()->profil == '4') {

            return view('agent.detailsDeclaration', compact('declaration', 'localisation'));
        } elseif (Auth::user()->profil == '5') {

            return view('officier.detailsDeclaration', compact('declaration', 'localisation'));
        }
    }
    public function ValidatedOfficier(Request $request, $id)
    {
        $declaration = Declaration::find($id);
        $declaration->delivrance = date("Y-m-d");
        $declaration->delivrer_par = Auth::user()->id;
        $declaration->statut = 2;
        $declaration->update();
        $message = "Bonjour veuillez répérer l'extrait de naissance";
        $this->sendSMS($declaration->telephone_mere, $message);
        return redirect('/officier/declaration')->with('success', 'Validé avec succés');
    }

    public function sendSMS($phone, $message)
    {
        $config = array(
            'clientId' => config('app.clientId'),
            'clientSecret' =>  config('app.clientSecret'),
        );

        $osms = new Sms($config);

        $data = $osms->getTokenFromConsumerKey();
        // $token = array(
        //     'token' => $data['access_token']
        // );


        $response = $osms->sendSms(
            // sender
            'tel:+221781766172',
            // receiver
            'tel:+221' . $phone,
            // message
            $message,
            'Devscom'
        );
    }
    public function pdf(Request $request, $id)
    {

        $declaration = Declaration::findOrFail($id);
        $localisation = DB::select('select r.id as id_r,r.nom as nom_r,u.id as id_u,u.username as username_u ,d.id as id_d,d.nom as nom_d,c.id as id_c,c.nom as nom_c,l.id as id_l,l.nom as nom_l from declarations dp ,ml_region r,ml_departement d ,ml_localite l ,ml_commune c,users u WHERE 1  AND dp.region=r.id AND dp.departement=d.id AND dp.localite=l.id AND dp.commune=c.id AND dp.delivrer_par=u.id');

        if (Auth::user()->profil == '1' or Auth::user()->profil == '2') {

            return view('pdf', compact('declaration', 'localisation'));
        } elseif (Auth::user()->profil == '3') {

            return view('declarant.detailsDeclaration', compact('declaration', 'localisation'));
        } elseif (Auth::user()->profil == '4') {

            return view('agent.detailsDeclaration', compact('declaration', 'localisation'));
        } elseif (Auth::user()->profil == '5') {

            return view('pdf', compact('declaration', 'localisation'));
        }
    }
    public function volet(Request $request, $id)
    {
        $declaration = Declaration::findOrFail($id);
        $localisation = DB::select('select r.id as id_r,r.nom as nom_r,u.id as id_u,u.username as username_u ,d.id as id_d,d.nom as nom_d,c.id as id_c,c.nom as nom_c,l.id as id_l,l.nom as nom_l from declarations dp ,ml_region r,ml_departement d ,ml_localite l ,ml_commune c,users u WHERE 1  AND dp.region=r.id AND dp.departement=d.id AND dp.localite=l.id AND dp.commune=c.id AND dp.delivrer_par=u.id');

        if (Auth::user()->profil == '1' or Auth::user()->profil == '2') {

            return view('pdf', compact('declaration', 'localisation'));
        } elseif (Auth::user()->profil == '3') {

            return view('declarant.detailsDeclaration', compact('declaration', 'localisation'));
        } elseif (Auth::user()->profil == '4') {

            return view('agent.detailsDeclaration', compact('declaration', 'localisation'));
        } elseif (Auth::user()->profil == '5') {

            return view('volet', compact('declaration', 'localisation'));
        }
    }
    public function export_excel_Officier()
    {

        return Excel::download(new DeclarationExport, 'declaration.xlsx');
    }
    public function export_excel_Agent()
    {

        return Excel::download(new DeclarationAgentExport, 'declaration.xlsx');
    }
}
