<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('auth.login');
});


Route::group(['middleware' => ['auth', '1']], function () {

	Route::get('/admin/home', 'HomeController@index');
	Route::get('/admin/utilisateur', 'UserController@index');
	Route::post('/admin/ajoutUtilisateur', 'UserController@store');
	Route::get('/admin/modification-utilisateur/{id}', 'UserController@update');
	Route::put('admin/update-utilisateur-saving/{id}', 'UserController@updateSaving');
	Route::post('/admin/deleteUser/{id}', 'UserController@delete');
	Route::post('/admin/activerUser/{id}', 'UserController@activer');
	Route::get('/admin/details-utilisateur/{id}', 'UserController@details');

	//declaration
	Route::get('/admin/declaration', 'DeclarationController@index');
	Route::post('/admin/ajoutDeclaration', 'DeclarationController@store');
	Route::get('/admin/modification-declaration/{id}', 'DeclarationController@update');
	Route::put('admin/update-declaration-saving/{id}', 'DeclarationController@updateSaving');
	Route::get('/admin/details-declaration/{id}', 'DeclarationController@details');
});


Route::get('admin/choixreg/{reg}', 'ChoixController@choixReg');
Route::get('admin/choixdept/{dept}', 'ChoixController@choixDept');
Route::get('admin/choixcomm/{comm}', 'ChoixController@choixComm');

Route::group(['middleware' => ['auth', '3']], function () {

	Route::get('/declarant/declaration', 'DeclarationController@index');
	Route::post('/declarant/ajoutDeclaration', 'DeclarationController@storeDeclarant');
	Route::get('/declarant/details-declaration/{id}', 'DeclarationController@details');
	Route::get('/declarant/modification-declaration/{id}', 'DeclarationController@update');
	Route::put('declarant/update-declaration-saving/{id}', 'DeclarationController@updateSavingDeclarant');
	
});

Route::group(['middleware' => ['auth', '4']], function () {
	Route::get('/agent/declaration', 'DeclarationController@index');
	Route::get('/agent/details-declaration/{id}', 'DeclarationController@details');
	Route::get('/agent/modification-declaration/{id}', 'DeclarationController@update');
	Route::put('agent/update-declaration-saving/{id}', 'DeclarationController@updateSavingAgent');
	Route::get('officier/export_agent', 'DeclarationController@export_excel_Agent')->name('export_agent');
	
});

Route::group(['middleware' => ['auth', '5']], function () {

	Route::get('/officier/declaration', 'DeclarationController@index');
	Route::get('/officier/details-declaration/{id}', 'DeclarationController@details');
	Route::get('/officier/modification-declaration/{id}', 'DeclarationController@update');
	Route::put('officier/update-declaration-saving/{id}', 'DeclarationController@updateSavingOfficier');
	Route::put('officier/valider/{id}', 'DeclarationController@ValidatedOfficier')->name('valider');
	Route::get('/officier/pdf/{id}', 'DeclarationController@pdf');
	Route::get('/officier/volet/{id}', 'DeclarationController@volet');
	Route::get('officier/export', 'DeclarationController@export_excel_Officier')->name('export');
	
});
Route::post('/password/change', 'UserController@changePassword');

Route::get('/admin/cartographie', function () {
	return view('admin.carte');
});
Auth::routes();
